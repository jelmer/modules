##############################################################################
#   Modules Revision 3.0
#   Providing a flexible user environment
#
#   File:		modules.90-avail/%M%
#   Revision:		%I%
#   First Edition:	2017/05/17
#   Last Mod.:		%U%, %G%
#
#   Authors:		Xavier Delaruelle, xavier.delaruelle@cea.fr
#
#   Description:	Testuite testsequence
#   Command:		avail
#   Modulefiles:
#   Sub-Command:
#
#   Comment:	%C{
#			Check the module 'avail' command on all existing modulefiles
#			with specific terminal width setup
#		}C%
#
##############################################################################

#
#  Check this only fo the /bin/sh
#

set test_cols 201
if {![info exists term_cols]} {
    # skip tests if current terminal witdh is unknown
    send_user "\tskipping terminal width-specific tests\n"
# set a specific terminal width
} elseif {[catch {exec stty cols $test_cols}]} {
    send_user "\tskipping terminal width-specific tests, cannot set width\n"
} else {

set test_cols_small 25
set test_cols_othos 120

set len  [string length $modpath]
set lrep [expr {($test_cols - $len - 2)/2}]
set rrep [expr {$test_cols - $len - 2 - $lrep}]
set ts_sh "[string repeat {-} $lrep] $modpath [string repeat {-} $rrep]
alias/1.0           info/shellsexp                 loc_sym/1.0                                                          modbad/is-avail      spread/4.0                                       
alias/2.0           info/specified(foo)            loc_sym/alias1                                                       modbad/is-loaded     spread/5.0                                       
alias/3.0           info/type                      loc_sym/alias2                                                       modbad/is-saved      spread/6.0                                       
append/0.1          info/user                      loc_sym/alias3                                                       modbad/is-used       spread/7.0                                       
append/0.2          info/userexp                   loc_sym/alias4                                                       modbad/path          spread/8.0                                       
append/0.3          inforc/1.0                     loc_sym/alias5                                                       modbad/paths         spreadrc/dir1/1.0                                
append/0.4          inforc/2.0(avail:bar:default)  loc_sym/alias6                                                       modbad/prepend-path  spreadrc/dir2/1.0                                
append/0.5          inforc/foo(@)                  loc_sym/alias7                                                       modbad/remove-path   spreadrc/dir3/1.0                                
append/1.0          load/00                        loc_sym/alias8                                                       module/2.0           spreadrc/dir4/1.0                                
append/1.1          load/10                        loc_sym/alias9                                                       module/bad           spreadrc/dir5/1.0                                
append/1.3          load/11                        loc_sym/alias10                                                      module/empty         spreadrc/dir6/1.0                                
append/1.4          load/12                        loc_sym/alias11                                                      module/err           spreadrc/dir7/1.0                                
append/1.5          load/13                        loc_sym/exec1                                                        module/lbad          spreadrc/dir8/1.0                                
append/1.6          load/14                        loc_sym/exec2                                                        module/lerr          symlink/0.9                                      
append/1.7          load/15                        loc_sym/exec3                                                        module/lunk          symlink/1(@)                                     
append/2.0          load/16                        loc_sym/getvers1                                                     module/meta          symlink/1.2(default:new)                         
append/2.1          load/17                        loc_sym/getvers2                                                     module/relpath       symlink/bar(@)                                   
append/2.2          load/18                        loc_sym/getvers3                                                     module/unk           symlink2/1.0                                     
append/2.3          load/19                        loc_sym/getvers4                                                     modvar/modfile       symlink2/2.0                                     
append/2.4          load/20                        loc_sym/getvers5                                                     modvar/submodfile    system/1.0                                       
append/4.0          load/21                        loc_sym/getvers6                                                     prepend/0.1          system/2.0                                       
append/4.1          load/22                        loc_sym/getvers7                                                     prepend/0.2          test/1.0                                         
append/5.0          load/23                        loc_sym/getvers8                                                     prepend/0.3          test/1.2                                         
append/6.0          load/24                        loc_sym/getvers9                                                     prepend/0.4          test/2.0                                         
averssort/1(@)      load/25                        loc_sym/getvers10                                                    prepend/0.5          tr2_loc(trreg)                                   
averssort/1.2.4(@)  load/26                        loc_sym/versinf1                                                     prepend/1.0          tr2_loc/al1(tr2unstable:@)                       
averssort/1.10(@)   load/27                        loc_sym/versinf2                                                     prepend/1.1          tr2_loc/al2(tr2bar:@)                            
bad/after(good)     load/28                        loc_sym/versinf3                                                     prepend/1.3          tr2_loc/al3(default:tr2exp:trreg:@)              
bad/before          load/29                        loc_sym/versinf4                                                     prepend/1.4          tr2_loc/al4(@)                                   
bad2/body           load/30                        loc_sym/versinf5                                                     prepend/1.5          tr2_loc/al5(default:trreg:@)                     
bad2/proc           load/all(default)              loc_sym/versinf6                                                     prepend/1.6          tr_loc/al1(tr2unstable:trunstable:@)             
break/1.0           loc_def/default                loc_sym/versinf7                                                     prepend/1.7          tr_loc/al2(default:tr2bar:tr2exp:trbar:trreg:@)  
break/2.0           loc_def/truedef                loc_sym/version1                                                     prepend/1.8          tr_loc/al3(trexp:@)                              
break/3.0           loc_dv1/1.0                    loc_sym/version2                                                     prepend/1.9          tr_loc/al4(@)                                    
break/4.0           loc_dv1/2.0                    loc_sym/version3                                                     prepend/2.0          tr_loc/al5(@)                                    
break/5.0           loc_dv2/1.0(default)           loc_sym/version4                                                     prepend/2.1          trace/all_off                                    
break/6.0           loc_dv2/2.0                    loc_sym/version5                                                     prepend/2.2          trace/all_on                                     
chdir/1.0           loc_dv3/1.0                    loc_sym/version6                                                     prepend/2.3          uname/cache                                      
chdir/2.0           loc_dv3/2.0                    loc_sym/version7                                                     prepend/2.4          uname/domain                                     
chdir/3.0           loc_dv4/1.0                    loc_sym/version8                                                     prepend/3.0          uname/machine                                    
chdir/4.0           loc_dv6/1.0                    loc_sym/version9                                                     prepend/3.1          uname/nodename                                   
coll/a              loc_dv6/2.0/1.0                loc_sym/version10                                                    prereq/full          uname/release                                    
coll/b              loc_dv7/1.0                    loc_sym/version11                                                    prereq/fullpath      uname/sysname                                    
coll/c              loc_dv7/2.0(default)           loc_sym/version12                                                    prereq/module        uname/unk                                        
coll/d              loc_dv7/2.0/1.0                loc_sym/version13                                                    prereq/orlist        uname/version                                    
conflict/full       loc_dv7/3.0                    loc_sym/version14                                                    prereq/relpath       unsetenv/0.8                                     
conflict/fullpath   loc_dv8/1.0                    loc_sym/version15                                                    puts/1               unsetenv/0.9                                     
conflict/module     loc_dv8/2.0                    loc_sym/version16                                                    puts/2               unsetenv/1.0                                     
conflict/relpath    loc_dv9/1.0(default)           loc_sym/version17                                                    puts/3               use/1.0(default)                                 
continue/1.0        loc_dv9/2.0                    loc_sym/version18                                                    puts/4               use/2.0                                          
continue/2.0        loc_dvv1/1.0(default)          loc_sym/version19                                                    puts/5               use/2.1                                          
continue/3.0        loc_dvv1/2.0                   loc_sym/version20                                                    puts/6               use/2.2                                          
continue/4.0        loc_fq/1.0                     loc_sym/version21                                                    puts/7               use/3.0                                          
continue/5.0        loc_rc1/1.0(foo)               loc_sym/version22                                                    puts/8               use/3.1                                          
continue/6.0        loc_rc1/2.0                    loc_sym/version23                                                    recurs/modA          use/3.2                                          
dirmodalias(@)      loc_rc2/1.0(bar:blah:foo)      loc_sym/version24                                                    recurs/modB          use/4.0                                          
dirmodalias/1.0     loc_rc2/2.0                    loc_sym/version25                                                    remove/0.3           use/4.1                                          
dirmodvirt          loc_rc3/1.0(default)           loc_sym/version26                                                    remove/0.4           user/adv                                         
dirmodvirt/1.0      loc_rc3/2.0(cur:stable)        loc_tr(reg)                                                          remove/0.5           user/advanced                                    
empty/1.0           loc_rc3/3.0(chk:exp:new:test)  loc_tr/1.0(cur:stable)                                               remove/1.0           user/exp                                         
eschars/1.0         loc_rc4/1.0                    loc_tr/2.0(next:tr2unstable:trunstable:unstable)                     remove/1.3           user/expert                                      
exit/1.0            loc_rc4/2.0(default)           loc_tr/3.0(bar:default:exp:foo:reg:tr2bar:tr2exp:trbar:trexp:trreg)  remove/1.4           user/nov                                         
exit/2.0            loc_rc4/3.0                    loc_tr/al1(unstable:@)                                               remove/1.5           user/novice                                      
exit/3.0            loc_rc5/1.0                    loc_tr/al2(bar:default:exp:reg:trexp:@)                              remove/1.6           user/undef                                       
exit/4.0            loc_rc5/2.0                    loc_tr/al3(default:exp:reg:@)                                        remove/1.7           verbose/msg                                      
getenv/1.0          loc_rc6/0.9                    loc_tr/al4(default:reg:@)                                            remove/2.0           verbose/off                                      
help/2.0            loc_rc6/1(@)                   loc_virt1/1.0                                                        remove/2.3           verbose/on                                       
info/command        loc_rc6/1.2(default:new)       loc_virt1/2.0                                                        remove/2.4           verbose/undef                                    
info/commandexp     loc_rc6/bar(@)                 loc_virt1/3.0                                                        remove/3.0           versions/1.1                                     
info/isavail        loc_rc7/0.9                    loc_virt1/4.0                                                        remove/3.1           versions/1.2                                     
info/isloaded       loc_rc7/1(@)                   loc_virt2/1.0                                                        remove/4.0           versions/1.3                                     
info/issaved        loc_rc7/1.2                    loc_virt2/2.0                                                        remove/4.1           verssort/1                                       
info/isused         loc_rc7/bar(@)                 log/badfac                                                           remove/4.2           verssort/1.2.1                                   
info/loaded         loc_rc8/0.9(@)                 log/err_both_1                                                       setenv/0.6           verssort/1.2.4                                   
info/mode1          loc_rc8/1.0                    log/err_both_2                                                       setenv/0.8           verssort/1.8-2015-12-01                          
info/mode2          loc_rcv1/1(@)                  log/err_file                                                         setenv/1.0           verssort/1.8-2016-02-01                          
info/mode3          loc_rcv1/1.1                   log/err_syslog                                                       source/0.9           verssort/1.10                                    
info/mode4          loc_rcv1/1.2(default:new)      modbad/append-path                                                   source/1.0           whatis/lines                                     
info/mode5          loc_rcv1/2.0                   modbad/autoinit                                                      source/1.1           whatis/multiple                                  
info/mode6          loc_rcv1/bar(@)                modbad/empty                                                         source/1.2           whatis/none                                      
info/name           loc_rcv2/1.2                   modbad/foo                                                           spread/1.0           whatis/single                                    
info/others         loc_rcv2/1.5                   modbad/help                                                          spread/2.0           whatis/string                                    
info/shells         loc_rcv2/2.0                   modbad/info-loaded                                                   spread/3.0           x-resource/1                                     "

set ts_sh_small "- $modpath.deep -
dir2/1.0(d1)                                          
dir2/3.0(d3)                                          
modalias/1.0(@)                                       
modalias/3.0                                          
modalias/dir1/1.0                                     
modalias/dir1/2.0(@)                                  
modalias/dir2/2.0                                     
modalias/dir2/3.0(@)                                  
moddalias(@)                                          
moddalias/dir1/1                                      
moddef/dir2/1.0(default)                              
moddef/dir2/2.0                                       
modload/dir2/1.0(md1)                                 
modload/dir2/3.0(md3)                                 
modsym/dir1/1.0(3.0)                                  
modsym/dir2/2.0(3.0)                                  
modtr/3.0(bar:exp:foo:reg:tr2bar:trbar)               
modtr/al1(unstable:@)                                 
modtr/al4(default:reg:@)                              
modtr/al5(fld:@)                                      
modtr/al6(sfld:@)                                     
modtr/dir1/1.0(cur:stable)                            
modtr/dir2/2.0(next:tr2unstable:trunstable:unstable)  
modtr/dir3/al2(bar:default:exp:reg:tr2bar:trbar:@)    
modtr/dir3/al3(default:exp:reg:@)                     
modtr/dir4(default:fld:reg:trfld:trreg)               
modtr/dir4/al7(madj:@)                                
modtr/dir4/al8(dadj:@)                                
modtr/dir4/mod4.0                                     
modtr/dir5/subdir(dadj:sfld)                          
modtr/dir5/subdir/mod5.0(madj)                        
modulerc/dir1(default)                                
modulerc/dir1/1.0(default)                            
modulerc/dir1/2.0                                     
modulerc/dir2/1.0(default)                            
modulerc/dir2/1.0/rc1(default)                        
modulerc/dir2/1.0/rc2                                 
modulerc/dir2/2.0                                     
modulerc/dir2/3.0                                     
modvirt/1.0(@)                                        
modvirt/3.0                                           
modvirt/dir0/sub1/4.0                                 
modvirt/dir0/sub1/5.0(@)                              
modvirt/dir0/sub1/6.0                                 
modvirt/dir1/1.0                                      
modvirt/dir1/2.0(@)                                   
modvirt/dir2/2.0                                      
modvirt/dir2/3.0(@)                                   
plain/dir1/1.0                                        
plain/dir1/2.0                                        
plain/dir2/1.0                                        
plain/dir2/2.0                                        
tr2mod/al1(tr2unstable:@)                             
tr2mod/al5(tr2fld:@)                                  
tr2mod/dir3/al2(tr2bar:@)                             
trmod(trreg)                                          
trmod/al1(tr2unstable:trunstable:@)                   
trmod/al5(default:trfld:trreg:@)                      
trmod/dir3(tr2fld)                                    
trmod/dir3/al2(tr2bar:trbar:@)                        
version/dir1(default)                                 
version/dir1/1.0(default)                             
version/dir1/2.0                                      
version/dir2/1.0                                      
version/dir2/2.0(default)                             
version/dir2/2.0/rc1(default)                         
version/dir2/2.0/rc2                                  
version/dir2/3.0                                      "

set ts_sh_noflag "- $modpath.deep -
dir2/1.0                  
dir2/3.0                  
modalias/1.0              
modalias/3.0              
modalias/dir1/1.0         
modalias/dir1/2.0         
modalias/dir2/2.0         
modalias/dir2/3.0         
moddalias                 
moddalias/dir1/1          
moddef/dir2/1.0           
moddef/dir2/2.0           
modload/dir2/1.0          
modload/dir2/3.0          
modsym/dir1/1.0           
modsym/dir2/2.0           
modtr/3.0                 
modtr/al1                 
modtr/al4                 
modtr/al5                 
modtr/al6                 
modtr/dir1/1.0            
modtr/dir2/2.0            
modtr/dir3/al2            
modtr/dir3/al3            
modtr/dir4                
modtr/dir4/al7            
modtr/dir4/al8            
modtr/dir4/mod4.0         
modtr/dir5/subdir         
modtr/dir5/subdir/mod5.0  
modulerc/dir1             
modulerc/dir1/1.0         
modulerc/dir1/2.0         
modulerc/dir2/1.0         
modulerc/dir2/1.0/rc1     
modulerc/dir2/1.0/rc2     
modulerc/dir2/2.0         
modulerc/dir2/3.0         
modvirt/1.0               
modvirt/3.0               
modvirt/dir0/sub1/4.0     
modvirt/dir0/sub1/5.0     
modvirt/dir0/sub1/6.0     
modvirt/dir1/1.0          
modvirt/dir1/2.0          
modvirt/dir2/2.0          
modvirt/dir2/3.0          
plain/dir1/1.0            
plain/dir1/2.0            
plain/dir2/1.0            
plain/dir2/2.0            
tr2mod/al1                
tr2mod/al5                
tr2mod/dir3/al2           
trmod                     
trmod/al1                 
trmod/al5                 
trmod/dir3                
trmod/dir3/al2            
version/dir1              
version/dir1/1.0          
version/dir1/2.0          
version/dir2/1.0          
version/dir2/2.0          
version/dir2/2.0/rc1      
version/dir2/2.0/rc2      
version/dir2/3.0          "

set len  [string length $modpath.deep]
set lrep [expr {($test_cols_othos - $len - 2)/2}]
set rrep [expr {$test_cols_othos - $len - 2 - $lrep}]
set ts_sh_noflag_othos "[string repeat {-} $lrep] $modpath.deep [string repeat {-} $rrep]
dir2/1.0           modsym/dir1/1.0  modtr/dir4/mod4.0         modvirt/dir0/sub1/5.0  trmod/al1             
dir2/3.0           modsym/dir2/2.0  modtr/dir5/subdir         modvirt/dir0/sub1/6.0  trmod/al5             
modalias/1.0       modtr/3.0        modtr/dir5/subdir/mod5.0  modvirt/dir1/1.0       trmod/dir3            
modalias/3.0       modtr/al1        modulerc/dir1             modvirt/dir1/2.0       trmod/dir3/al2        
modalias/dir1/1.0  modtr/al4        modulerc/dir1/1.0         modvirt/dir2/2.0       version/dir1          
modalias/dir1/2.0  modtr/al5        modulerc/dir1/2.0         modvirt/dir2/3.0       version/dir1/1.0      
modalias/dir2/2.0  modtr/al6        modulerc/dir2/1.0         plain/dir1/1.0         version/dir1/2.0      
modalias/dir2/3.0  modtr/dir1/1.0   modulerc/dir2/1.0/rc1     plain/dir1/2.0         version/dir2/1.0      
moddalias          modtr/dir2/2.0   modulerc/dir2/1.0/rc2     plain/dir2/1.0         version/dir2/2.0      
moddalias/dir1/1   modtr/dir3/al2   modulerc/dir2/2.0         plain/dir2/2.0         version/dir2/2.0/rc1  
moddef/dir2/1.0    modtr/dir3/al3   modulerc/dir2/3.0         tr2mod/al1             version/dir2/2.0/rc2  
moddef/dir2/2.0    modtr/dir4       modvirt/1.0               tr2mod/al5             version/dir2/3.0      
modload/dir2/1.0   modtr/dir4/al7   modvirt/3.0               tr2mod/dir3/al2        
modload/dir2/3.0   modtr/dir4/al8   modvirt/dir0/sub1/4.0     trmod                  "

#
#  test
#

testouterr_cmd "sh" "avail" "OK" "$ts_sh"

# setup MODULEPATH with a trailing slash and an empty dir element
if { $verbose > 0 } {
    send_user "\tSetup MODULEPATH = :$modpath/\n"
}
set env(MODULEPATH) ":$modpath/"
testouterr_cmd "sh" "avail" "OK" "$ts_sh"


# test with a small terminal width
exec stty cols $test_cols_small
if { $verbose > 0 } {
    send_user "\tSetup MODULEPATH = $modpath.deep\n"
}
set env(MODULEPATH) "$modpath.deep"
testouterr_cmd "sh" "avail" "OK" "$ts_sh_small"


# check excepted siteconfig file is installed
if {[siteconfig_isStderrTty]} {

# test output when flag report is disabled
if { $verbose > 0 } {
    send_user "\tSetup TESTSUITE_ENABLE_SITECONFIG_NO_FLAG = '1'\n"
}
set env(TESTSUITE_ENABLE_SITECONFIG_NO_FLAG) 1

testouterr_cmd "sh" "avail" "OK" "$ts_sh_noflag"

# simulate Solaris OS with its specific stty command output
if { $verbose > 0 } {
    send_user "\tSetup TESTSUITE_ENABLE_SITECONFIG_SOLARIS_STTY = '1'\n"
}
set env(TESTSUITE_ENABLE_SITECONFIG_SOLARIS_STTY) 1

testouterr_cmd "sh" "avail" "OK" "$ts_sh_noflag_othos"


# simulate Windows OS with its specific mode command output
if { $verbose > 0 } {
    send_user "\tSetup TESTSUITE_ENABLE_SITECONFIG_WINDOWS_MODE = '1'\n"
}
set env(TESTSUITE_ENABLE_SITECONFIG_WINDOWS_MODE) 1

testouterr_cmd "sh" "avail" "OK" "$ts_sh_noflag_othos"

# restore environment
if { $verbose > 0 } {
    send_user "\tUnset TESTSUITE_ENABLE_SITECONFIG_NO_FLAG\n"
    send_user "\tUnset TESTSUITE_ENABLE_SITECONFIG_SOLARIS_STTY\n"
    send_user "\tUnset TESTSUITE_ENABLE_SITECONFIG_WINDOWS_MODE\n"
}
unset env(TESTSUITE_ENABLE_SITECONFIG_NO_FLAG)
unset env(TESTSUITE_ENABLE_SITECONFIG_SOLARIS_STTY)
unset env(TESTSUITE_ENABLE_SITECONFIG_WINDOWS_MODE)

} elseif {$verbose > 0} {
   send_user "\tSkip some tests as excepted siteconfig file not installed\n"
}


#
#  Cleanup
#

# restore MODULEPATH
if { $verbose > 0 } {
    send_user "\tRestore MODULEPATH = $modpath\n"
}
set env(MODULEPATH) $modpath

# restore terminal width
exec stty cols $term_cols

unset ts_sh
unset ts_sh_small
unset ts_sh_noflag
unset ts_sh_noflag_othos

unset test_cols_small
unset test_cols_othos
unset len
unset lrep
unset rrep

}

unset test_cols
