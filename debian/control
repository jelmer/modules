Source: modules
Section: devel
Priority: optional
Maintainer: Alastair McKinstry <mckinstry@debian.org>
Standards-Version: 4.1.4
Homepage: http://modules.sourceforge.net/
Vcs-Browser: https://salsa.debian.org:/mckinstry/modules.git
Vcs-Git: https://salsa.debian.org:/mckinstry/modules.git
Build-Depends: tcl8.6-dev | tcl-dev, 
  debhelper (>= 10), 
  dejagnu, 
  tclx8.4-dev,
  less,
  procps

Package: environment-modules
Architecture: any
Depends: tcl8.6 | tcl, ${misc:Depends}, ${shlibs:Depends},
 less, procps
Conflicts: lmod
Description: Modular system for handling environment variables
 The Modules package provides for the dynamic modification of a user's
 environment via modulefiles.  Each modulefile contains the information
 needed to configure the shell for an application. Once the Modules
 package is initialized, the environment can be modified dynamically
 on a per-module basis using the module command which interprets
 modulefiles. Typically modulefiles instruct the module command to alter or
 set shell environment variables such as PATH, MANPATH, etc. modulefiles
 may be shared by many users on a system and users may have their own
 collection to supplement or replace the shared modulefiles.  The modules
 environment is common on SGI/Crays and many workstation farms.
